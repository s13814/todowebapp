﻿$(function () {
    var path = window.location.pathname;

    if (path == "/Home/Add" || path.startsWith("/Home/Edit")) {
        $('#collapsible-editor').collapsible('open', 0);
    }
});