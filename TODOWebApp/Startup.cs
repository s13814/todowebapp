﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TODOWebApp.Startup))]
[assembly: log4net.Config.XmlConfigurator(ConfigFile = "Web.config", Watch = true)]
namespace TODOWebApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
