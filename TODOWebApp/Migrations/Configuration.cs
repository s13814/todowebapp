namespace TODOWebApp.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using TODOWebApp.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<TODOWebApp.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "TODOWebApp.Models.ApplicationDbContext";
        }

        protected override void Seed(TODOWebApp.Models.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
            //context.State.AddOrUpdate(new State{ Name = "Scheduled" });
            //context.State.AddOrUpdate(new State { Name = "Done" });
            //context.State.AddOrUpdate(new State { Name = "Canceled" });
        }
    }
}
