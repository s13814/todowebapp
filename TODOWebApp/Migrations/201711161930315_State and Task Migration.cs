namespace TODOWebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class StateandTaskMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.States",
                c => new
                    {
                        StateID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 20),
                    })
                .PrimaryKey(t => t.StateID);
            
            CreateTable(
                "dbo.Tasks",
                c => new
                    {
                        TaskId = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 1024),
                        FromeDateTime = c.DateTime(nullable: false),
                        ToDateTime = c.DateTime(nullable: false),
                        Description = c.String(),
                        StateId = c.Int(nullable: false),
                        Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.TaskId)
                .ForeignKey("dbo.AspNetUsers", t => t.Id)
                .ForeignKey("dbo.States", t => t.StateId, cascadeDelete: true)
                .Index(t => t.StateId)
                .Index(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Tasks", "StateId", "dbo.States");
            DropForeignKey("dbo.Tasks", "Id", "dbo.AspNetUsers");
            DropIndex("dbo.Tasks", new[] { "Id" });
            DropIndex("dbo.Tasks", new[] { "StateId" });
            DropTable("dbo.Tasks");
            DropTable("dbo.States");
        }
    }
}
