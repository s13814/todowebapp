namespace TODOWebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedEventIdToTask : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Tasks", name: "Id", newName: "ApplicationUserId");
            RenameIndex(table: "dbo.Tasks", name: "IX_Id", newName: "IX_ApplicationUserId");
            AddColumn("dbo.Tasks", "EventId", c => c.String(maxLength: 1024));
            AddColumn("dbo.Tasks", "FromDateTime", c => c.DateTime(nullable: false));
            DropColumn("dbo.Tasks", "FromeDateTime");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Tasks", "FromeDateTime", c => c.DateTime(nullable: false));
            DropColumn("dbo.Tasks", "FromDateTime");
            DropColumn("dbo.Tasks", "EventId");
            RenameIndex(table: "dbo.Tasks", name: "IX_ApplicationUserId", newName: "IX_Id");
            RenameColumn(table: "dbo.Tasks", name: "ApplicationUserId", newName: "Id");
        }
    }
}
