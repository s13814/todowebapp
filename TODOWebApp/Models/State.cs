﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TODOWebApp.Models
{
    public class State
    {
        [Key]
        public int StateID { get; set; }

        [Required]
        [MaxLength(20)]
        public string Name { get; set; }

        public virtual List<Task> Tasks { get; set; }
    }
}