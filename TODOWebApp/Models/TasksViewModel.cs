﻿using System;
using System.Collections.Generic;

namespace TODOWebApp.Models
{
    public class TasksViewModel
    {
        public IEnumerable<Task> Tasks { get; set; }
        public Task Task { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime TimeFrom { get; set; }
        public DateTime DateTo { get; set; }
        public DateTime TimeTo { get; set; }
        public OperationData OperationData { get; set; }
    }

    public class OperationData
    {
        public string HeaderName { get; set; }
        public string PartialName { get; set; }
    }
}