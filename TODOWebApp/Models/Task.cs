﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TODOWebApp.Models
{
    public class Task
    {
        [Key]
        public int TaskId { get; set; }

        [MinLength(5)]
        [MaxLength(1024)]
        public string EventId { get; set; }
        
        [MaxLength(1024)]
        public string Name { get; set; }

        [Required]
        public DateTime FromDateTime { get; set; }

        [Required]
        public DateTime ToDateTime { get; set; }

        [MaxLength(8192)]
        public string Description { get; set; }

        public int StateId { get; set; }

        [ForeignKey("StateId")]
        public virtual State State { get; set; }

        public string ApplicationUserId { get; set; }

        [ForeignKey("ApplicationUserId")]
        public virtual ApplicationUser ApplicationUser { get; set; }
    }
}