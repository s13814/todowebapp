﻿using System.Linq;
using System.Web.Mvc;
using TODOWebApp.DAL.GoogleApi;
using Microsoft.AspNet.Identity;
using System;
using TODOWebApp.Models;
using System.Data.Entity;

namespace TODOWebApp.Controllers
{
    public class HomeController : Controller, IApplicationDataFlowProvider
    {
        private readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private ApplicationDbContext _context = new ApplicationDbContext();
        private string userId = System.Web.HttpContext.Current.User.Identity.GetUserId();
        private static string ADD_PARTIAL_NAME = "_AddTaskPartial";
        private static string EDIT_PARTIAL_NAME = "_EditTaskPartial";
        private static OperationData AddOperationData = new OperationData { PartialName = ADD_PARTIAL_NAME, HeaderName = "add" };
        private static OperationData EditOperationData = new OperationData { PartialName = EDIT_PARTIAL_NAME, HeaderName = "edit" };
        private GoogleCalendarSynchronizationProvider GoogleCalendarSynchronizationProvider;

        public HomeController()
        {
            GoogleCalendarSynchronizationProvider = new GoogleCalendarSynchronizationProvider(this);
        }

        public ActionResult Index()
        {
            var tasksFromDatabase = _context.Tasks.Where(task => task.ApplicationUserId == userId).ToList();
            logger.Info("Retrieved tasks from database");

            if (IsUserLoggedWithGoogle()) {
                tasksFromDatabase = GoogleCalendarSynchronizationProvider.Synchronize(tasksFromDatabase).ToList();
            }

            return View(new TasksViewModel { Tasks = tasksFromDatabase, OperationData = AddOperationData });
        }


        [HttpPost]
        public ActionResult Add(TasksViewModel model)
        {
            model.OperationData = AddOperationData;
            InitializeRemainingModelFields(model);

            if (!IsModelValid(model.Task))
            {
                return View("Index", model);
            }

            if (IsUserLoggedWithGoogle())
            {
                var createdEvent = GoogleCalendarSynchronizationProvider.CreateGoogleCalendarEvent(model.Task);
                model.Task.EventId = createdEvent.Id;
            }
            
            AddTaskToDatabase(model.Task);
            logger.Info("Successfully added task [id=" + model.Task.TaskId + ", name=" + model.Task.Name + "] to database");
            
            return RedirectToAction("Index");
        }
        
        private void AddTaskToDatabase(Task task)
        {
            _context.Tasks.Add(task);
            _context.SaveChanges();
        }


        public ActionResult Edit(int taskId)
        { 
            var tasks = _context.Tasks.Where(task => task.ApplicationUserId == userId).ToList();
            logger.Info("Retrieved tasks from database");
            var taskToUpdate = _context.Tasks.First(task => task.TaskId == taskId);
            logger.Info("Retrieved task to update from database");

            var viewModel = new TasksViewModel
            {
                Tasks = tasks,
                Task = taskToUpdate,
                DateFrom = taskToUpdate.FromDateTime.Date,
                TimeFrom = taskToUpdate.FromDateTime.ToLocalTime(),
                DateTo = taskToUpdate.ToDateTime.Date,
                TimeTo = taskToUpdate.ToDateTime.ToLocalTime(),
                OperationData = new OperationData { PartialName = EDIT_PARTIAL_NAME, HeaderName = "edit" }
            };

            return View("Index", viewModel);
        }


        [HttpPost]
        public ActionResult Edit(TasksViewModel model)
        {
            model.OperationData = EditOperationData;
            InitializeRemainingModelFields(model);

            if (!IsModelValid(model.Task))
            {
                return View("Index", model);
            }

            var taskToUpdate = PrepareTaskToUpdate(model.Task);

            UpdateTaskInDatabase(taskToUpdate);
            logger.Info("Successfully updated task in Database [id=" + model.Task.TaskId + ", name=" + model.Task.Name + "]");

            if (IsUserLoggedWithGoogle())
            {
                GoogleCalendarSynchronizationProvider.UpdateGoogleCalendarEvent(taskToUpdate);
            }

            return RedirectToAction("Index");
        }

        private void UpdateTaskInDatabase(Task taskToUpdate)
        {
            _context.Entry(taskToUpdate).State = System.Data.Entity.EntityState.Modified;
            _context.SaveChanges();
        }

        public ActionResult Delete(int taskId)
        {
            var taskToDelete = _context.Tasks.First(task => task.TaskId == taskId);
            logger.Info("Retrieved task to remove from database");

            DeleteTaskFromDatabase(taskToDelete);
            logger.Info("Successfully deleted task from Database [id=" + taskId + ", name=" + taskToDelete.Name + "]");

            if (IsUserLoggedWithGoogle())
            {
                GoogleCalendarSynchronizationProvider.DeleteGoogleCalendarEvent(taskToDelete);
            }

            return RedirectToAction("Index");
        }

        private void DeleteTaskFromDatabase(Task taskToDelete)
        {
            _context.Tasks.Remove(taskToDelete);
            _context.SaveChanges();
        }

        public ActionResult MarkTaskSupplyingStateId(int taskId, int stateId)
        {
            var taskToMark = _context.Tasks.First(task => task.TaskId == taskId);
            logger.Info("Retrieved task to mark from database");
            taskToMark.StateId = stateId;

            UpdateTaskInDatabase(taskToMark);
            logger.Info("Successfully remarked task [id=" + taskId + ", name=" + taskToMark.Name + "]");

            return RedirectToAction("Index");
        }


        private Task PrepareTaskToUpdate(Task updatedTask)
        {
            var taskToUpdate = _context.Tasks.First(t => t.TaskId == updatedTask.TaskId);
            logger.Info("Retrieved task to update from database");
            taskToUpdate.ApplicationUser = updatedTask.ApplicationUser;
            taskToUpdate.Description = updatedTask.Description;
            taskToUpdate.FromDateTime = updatedTask.FromDateTime;
            taskToUpdate.ApplicationUserId = updatedTask.ApplicationUserId;
            taskToUpdate.Name = updatedTask.Name;
            taskToUpdate.State = updatedTask.State;
            taskToUpdate.StateId = updatedTask.StateId;
            taskToUpdate.TaskId = updatedTask.TaskId;
            taskToUpdate.ToDateTime = updatedTask.ToDateTime;

            return taskToUpdate;
        }


        private void InitializeRemainingModelFields(TasksViewModel model)
        {
            model.Task.ApplicationUserId = userId;
            model.Task.ApplicationUser = _context.Users.First(user => user.Id == userId);
            model.Task.State = _context.State.First(state => state.StateID == 1);
            model.Task.StateId = 1; // scheduled
            model.Task.FromDateTime = CombineDateAndTimeIntoDateTime(model.DateFrom, model.TimeFrom);
            model.Task.ToDateTime = CombineDateAndTimeIntoDateTime(model.DateTo, model.TimeTo);
            model.Tasks = _context.Tasks.Where(task => task.ApplicationUserId == userId).ToList();

            LogGettingData();
        }


        private DateTime CombineDateAndTimeIntoDateTime(DateTime modelDate, DateTime modelTime)
        {
            return modelDate.Date.Add(modelTime.TimeOfDay);
        }


        private void LogGettingData()
        {
            logger.Info("Retrieved data from database: [ApplicationUser, State, Tasks]");
        }


        private bool IsModelValid(Task task)
        {
            return ModelState.IsValid && AreDatesValid(task);
        }


        private bool AreDatesValid(Task task)
        {
            if (task.FromDateTime < DateTime.Now.AddMinutes(-15))
            {
                ViewBag.timeError = "Event starting time must not be from the past";
                return false;
            }
            if (task.FromDateTime > task.ToDateTime)
            {
                ViewBag.dateError = "Event ending date must be after starting date";
                return false;
            }

            return true;
        }


        public ActionResult Contact()
        {
            return View();
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool IsUserLoggedWithGoogle()
        {
            return UserCredentialProvider.IsUserLoggedWithGoogle(this);
        }

        public string GetUserId()
        {
            return this.userId;
        }

        public DbContext GetContext()
        {
            return this._context;
        }
    }
}