﻿using Google.Apis.Services;
using Google.Apis.Calendar.v3;

namespace TODOWebApp.DAL.GoogleApi
{
    internal class GoogleCalendarService : CalendarService
    {
        private GoogleCalendarService(Initializer initializer) : base(initializer)
        {
        }

        public static GoogleCalendarService GetService(string loggedWithGoogleUserId)
        {
            var credential = UserCredentialProvider.GetCredentialForGoogleApi(loggedWithGoogleUserId);

            var initializer = new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = "TODOWebApp",
            };

            return new GoogleCalendarService(initializer);
        }
    }
}