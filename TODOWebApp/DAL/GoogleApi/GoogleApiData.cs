﻿using Google.Apis.Calendar.v3;

namespace TODOWebApp.DAL.GoogleApi
{
    public class GoogleApiData : ApiData
    {
        public override string ClientId => "105973704436-sc7iukp6t2fta2r715qtimb5jups4abn.apps.googleusercontent.com";
        public override string ClientSecret => "q2_NznwWS4geATsMMiJEs6TQ";
        public override string UserId => "GoogleUserId";
        public override string[] Scopes
        {
            get
            {
                return new[]{
                    "openid",
                    "email",
                    CalendarService.Scope.Calendar
                };
            }
        }
    }
}