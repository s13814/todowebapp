﻿using Google.Apis.Calendar.v3.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TODOWebApp.Controllers;
using TODOWebApp.Models;

namespace TODOWebApp.DAL.GoogleApi
{
    public class GoogleCalendarSynchronizationProvider
    {
        private static readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private IApplicationDataFlowProvider controller;
        private ApplicationDbContext _context;
        private static string loggedWithGoogleUserId;
        private IList<Task> tasksFromDatabase;
        private IList<Task> tasksFromGoogleCalendar;

        public GoogleCalendarSynchronizationProvider(IApplicationDataFlowProvider controller)
        {
            this.controller = controller;
            this._context = (ApplicationDbContext)this.controller.GetContext();
        }


        internal IEnumerable<Task> RetrieveTasks()
        {
            loggedWithGoogleUserId = UserCredentialProvider.GetLoggedWithGooleUserId((Controller)controller);

            var calendarService = GoogleCalendarService.GetService(loggedWithGoogleUserId);
            var events = calendarService.Events.List("primary").Execute().Items;

            var tasks = CreateTasksFromEvents(events);
            
            return tasks;
        }


        private IEnumerable<Task> CreateTasksFromEvents(IList<Event> events)
        {
            var userId = ((IApplicationDataFlowProvider)controller).GetUserId();

            // TODO PROBLEM Z DATAMI JAK JEST WYDARZENIE CALODNIOWE

            var tasks = events.Select(evt => new Task
                {
                    ApplicationUserId = userId,
                    EventId = evt.Id,
                    StateId = 1,
                    Name = evt.Summary,
                    FromDateTime = evt.Start.DateTime.Value,
                    ToDateTime = evt.End.DateTime.Value,
                    Description = evt.Description
                });
            
            return tasks;
        }


        internal Event CreateGoogleCalendarEvent(Task task)
        {
            var calendarService = GoogleCalendarService.GetService(loggedWithGoogleUserId);

            var calendarEvent = new Event
            {
                Id = task.EventId,
                Summary = task.Name,
                Start = new EventDateTime { DateTime = task.FromDateTime },
                End = new EventDateTime { DateTime = task.ToDateTime },
                Description = task.Description
            };

            Event eventsResource = calendarService.Events.Insert(calendarEvent, "primary").Execute();

            if (eventsResource != null)
            {
                logger.Info("Successfully added task to Google Calendar [id=" + task.TaskId + ", name=" + task.Name + "]");
            }
            else
            {
                logger.Info("Adding task to Google Calendar failed [id=" + task.TaskId + ", name=" + task.Name + "]");
            }

            return eventsResource;
        }


        internal Event UpdateGoogleCalendarEvent(Task task)
        {
            var calendarService = GoogleCalendarService.GetService(loggedWithGoogleUserId);

            var calendarEvent = new Event
            {
                Id = task.EventId,
                Summary = task.Name,
                Start = new EventDateTime { DateTime = task.FromDateTime },
                End = new EventDateTime { DateTime = task.ToDateTime },
                Description = task.Description
            };
            
            Event eventsResource = calendarService.Events.Update(calendarEvent, "primary", calendarEvent.Id).Execute();

            if (eventsResource != null)
            {
                logger.Info("Successfully updated task in Google Calendar [id=" + task.TaskId + ", name=" + task.Name + "]");
            }
            else
            {
                logger.Info("Updating task in Google Calendar failed [id=" + task.TaskId + ", name=" + task.Name + "]");
            }

            return eventsResource;
        }


        internal void DeleteGoogleCalendarEvent(Task task)
        {
            var calendarService = GoogleCalendarService.GetService(loggedWithGoogleUserId);

            string responseBody = calendarService.Events.Delete("primary", task.EventId).Execute();

            if (responseBody == "")
            {
                logger.Info("Successfully deleted task from Google Calendar [id=" + task.TaskId + ", name=" + task.Name + "]");
            }
            else
            {
                logger.Info("Deleting task from Google Calendar failed [id=" + task.TaskId + ", name=" + task.Name + "]");
            }
        }

        
        private bool AreEqual(Task task1, Task task2)
        {
            return task1.EventId == task2.EventId &&
                   task1.Name == task2.Name &&
                   task1.FromDateTime == task2.FromDateTime &&
                   task1.ToDateTime == task2.ToDateTime &&
                   task1.Description == task2.Description;
        }


        internal IEnumerable<Task> Synchronize(List<Task> tasksFromDatabase)
        {
            this.tasksFromDatabase = tasksFromDatabase;
            this.tasksFromGoogleCalendar = this.RetrieveTasks().ToList();
            logger.Info("Retrieved tasks from Google Calendar");

            SynchronizeCalendarWithDatabase();
            SynchronizeDatabaseWithCalendar();

            return this.tasksFromDatabase;
        }
        

        private void SynchronizeCalendarWithDatabase()
        {
            foreach (Task taskFromDatabase in tasksFromDatabase)
            {
                AddEventToCalendarIfExistsInDatabaseButNotInCalendar(taskFromDatabase);
                RemoveTaskFromDatabaseIfNoLongerExistsInCalendar(taskFromDatabase);
            }
        }


        private void AddEventToCalendarIfExistsInDatabaseButNotInCalendar(Task taskFromDatabase)
        {
            if (taskFromDatabase.EventId == null)
            {
                Event newEvent = this.CreateGoogleCalendarEvent(taskFromDatabase);

                logger.Debug("Created task in calendar and Got evtid FROM CALENDAR to update " +
                             "[id=" + taskFromDatabase.TaskId +
                             ", name=" + taskFromDatabase.Name +
                             ", EventId=" + taskFromDatabase.EventId + "]");

                taskFromDatabase.EventId = newEvent.Id;

                UpdateTaskInDatabase(taskFromDatabase);

                // Dodanie do listy w celu podtrzymania synchronizacji bez koniecznosci pobierania danych z bazy/kalendarza za kazdym krokiem petli
                Task taskAddedToCalendar = CreateTasksFromEvents(new List<Event> { newEvent }).First();
                tasksFromGoogleCalendar.Add(taskAddedToCalendar);
            }
        }


        private void RemoveTaskFromDatabaseIfNoLongerExistsInCalendar(Task taskFromDatabase)
        {
            bool isInCal = tasksFromGoogleCalendar.Any(task => task.EventId == taskFromDatabase.EventId);
            if (!isInCal)
            {
                RemoveTaskFromDatabase(taskFromDatabase);
            }
        }

        private void RemoveTaskFromDatabase(Task taskFromDatabase)
        {
            _context.Tasks.Remove(taskFromDatabase);
            _context.SaveChanges();

            logger.Debug("Del task from DB bcoz it no longer exists in calendar " +
                         "[id=" + taskFromDatabase.TaskId +
                         ", name=" + taskFromDatabase.Name + "]");
        }


        private void SynchronizeDatabaseWithCalendar()
        {
            foreach (Task taskFromCalendar in tasksFromGoogleCalendar)
            {
                AddTaskToDatabaseIfExistsOnlyInCalendar(taskFromCalendar);
                UpdateTaskInDatabaseIfChangedInCalendar(taskFromCalendar);
            }
        }


        private void AddTaskToDatabaseIfExistsOnlyInCalendar(Task taskFromCalendar)
        {
            bool isInDb = tasksFromDatabase.Any(task => task.EventId == taskFromCalendar.EventId);

            logger.Debug("Is task from calendar in DB? -> " + isInDb + 
                         " [userId = " + taskFromCalendar.ApplicationUserId + 
                         ", name = " + taskFromCalendar.Name + 
                         ", EventId = " + taskFromCalendar.EventId + "]");

            if (!isInDb)
            {
                taskFromCalendar.TaskId = CreateNewTaskId(taskFromCalendar);
                taskFromCalendar.ApplicationUserId = ((IApplicationDataFlowProvider)controller).GetUserId();
                taskFromCalendar.StateId = 1;

                AddTaskToDatabase(taskFromCalendar);
            }
        }


        private int CreateNewTaskId(Task taskFromCalendar)
        {
            try
            {
                taskFromCalendar.TaskId = tasksFromDatabase.Max(task => task.TaskId) + 1;
            }
            catch (System.InvalidOperationException ioe)
            {
                taskFromCalendar.TaskId = 1;
                logger.Debug("No elements, exception catched, id will be 1");
            }

            logger.Debug("Retrieved tasks's new id from calendar -> " + taskFromCalendar.TaskId);

            return taskFromCalendar.TaskId;
        }


        private void AddTaskToDatabase(Task taskFromCalendar)
        {
            logger.Debug("trying add task from googlecalendar " +
                             "[userId =" + taskFromCalendar.ApplicationUserId +
                             ", id=" + taskFromCalendar.TaskId +
                             ", name=" + taskFromCalendar.Name +
                             ", EventId=" + taskFromCalendar.EventId + "] to database");

            _context.Tasks.Add(taskFromCalendar);
            _context.SaveChanges();

            // Dodanie do listy w celu podtrzymania synchronizacji bez koniecznosci pobierania danych z bazy/kalendarza za kazdym krokiem petli
            tasksFromDatabase.Add(taskFromCalendar);

            logger.Debug("Successfully added task [id=" + taskFromCalendar.TaskId + ", name=" + taskFromCalendar.Name + "] to database");
        }


        private void UpdateTaskInDatabaseIfChangedInCalendar(Task taskFromCalendar)
        {
            var taskFromDatabase = tasksFromDatabase.FirstOrDefault(task => task.EventId == taskFromCalendar.EventId);

            logger.Debug("Is task from calendar in DB? -> " + taskFromDatabase != null +
                         " [userId = " + taskFromCalendar.ApplicationUserId +
                         ", name = " + taskFromCalendar.Name +
                         ", EventId = " + taskFromCalendar.EventId + "]");

            if (IsTaskChangedInCalendar(taskFromDatabase, taskFromCalendar))
            {
                taskFromDatabase = PrepareTaskToUpdate(taskFromDatabase, taskFromCalendar);

                UpdateTaskInDatabase(taskFromDatabase);
            }
        }


        private bool IsTaskChangedInCalendar(Task taskFromDatabase, Task taskFromCalendar)
        {
            return taskFromDatabase != null && !AreEqual(taskFromDatabase, taskFromCalendar);
        }


        private Task PrepareTaskToUpdate(Task taskToUpdate, Task updatedTask)
        {
            taskToUpdate.Name = updatedTask.Name;
            taskToUpdate.Description = updatedTask.Description;
            taskToUpdate.FromDateTime = updatedTask.FromDateTime;
            taskToUpdate.ToDateTime = updatedTask.ToDateTime;

            return taskToUpdate;
        }


        private void UpdateTaskInDatabase(Task task)
        {
            _context.Entry(task).State = System.Data.Entity.EntityState.Modified;
            _context.SaveChanges();

            logger.Debug("Updated task in database" +
                         "[id=" + task.TaskId +
                         ", name=" + task.Name +
                         ", EventId=" + task.EventId + "]");
        }
    }
}