﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Auth.OAuth2.Flows;
using Google.Apis.Auth.OAuth2.Responses;
using Google.Apis.Util.Store;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace TODOWebApp.DAL.GoogleApi
{
    public static class UserCredentialProvider
    {
        private static readonly IDataStore dataStore = new FileDataStore(GoogleWebAuthorizationBroker.Folder);

        public static UserCredential GetCredentialForGoogleApi(string loggedWithGoogleUserId)
        {
            var initializer = new GoogleAuthorizationCodeFlow.Initializer
            {
                ClientSecrets = new ClientSecrets
                {
                    ClientId = ApiDataProvider.GoogleApiData.ClientId,
                    ClientSecret = ApiDataProvider.GoogleApiData.ClientSecret
                },
                Scopes = ApiDataProvider.GoogleApiData.Scopes,
            };

            var flow = new GoogleAuthorizationCodeFlow(initializer);

            var token = Task.Run(async () => await dataStore.GetAsync<TokenResponse>(loggedWithGoogleUserId)).Result;

            //var token = await dataStore.GetAsync<TokenResponse>(userId);

            return new UserCredential(flow, loggedWithGoogleUserId, token);
        }

        internal static string GetLoggedWithGooleUserId(Controller controller)
        {
            var externalIdentity = GetExternalIdentity(controller);

            return externalIdentity.FindFirstValue(ApiDataProvider.GoogleApiData.UserId);
        }

        private static ClaimsIdentity GetExternalIdentity(Controller controller)
        {
            return controller.HttpContext
                             .GetOwinContext()
                             .Authentication
                             .GetExternalIdentity(DefaultAuthenticationTypes.ApplicationCookie);
        }

        internal static bool IsUserLoggedWithGoogle(Controller controller)
        {
            return GetLoggedWithGooleUserId(controller) != null;
        }
    }
}