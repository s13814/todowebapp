﻿using System.Data.Entity;

namespace TODOWebApp.Controllers
{
    public interface IApplicationDataFlowProvider
    {
        string GetUserId();
        DbContext GetContext();
    }
}