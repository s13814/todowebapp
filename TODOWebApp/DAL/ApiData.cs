﻿
namespace TODOWebApp.DAL.GoogleApi
{
    public abstract class ApiData
    {
        public abstract string ClientId { get; }
        public abstract string ClientSecret { get; }
        public abstract string UserId { get; }
        public abstract string[] Scopes { get; }
    }
}