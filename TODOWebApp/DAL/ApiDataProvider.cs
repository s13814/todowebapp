﻿
namespace TODOWebApp.DAL.GoogleApi
{
    internal class ApiDataProvider
    {
        static ApiData googleApiData = new GoogleApiData();

        public static ApiData GoogleApiData {
            get
            {
                return googleApiData;
            }
        }
    }
}